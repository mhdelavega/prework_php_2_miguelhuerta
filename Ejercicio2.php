<?php

$frase = " Esto es una prueba";
$chars = str_split($frase);
$contieneA = 0; $contieneE = 0; $contieneI = 0; $contieneO = 0; $contieneU = 0; 
foreach ($chars as $letra) {
    switch ($letra) {
        case 'a':
            $contieneA = 1;
            break;
        case 'e':
            $contieneE = 1;
            break;
        case 'i':
            $contieneI = 1;
            break;
        case 'o':
            $contieneO = 1;
            break;
        case 'u':
            $contieneU = 1;
            break;
    }
}
if ($contieneA && $contieneE && $contieneI && $contieneO && $contieneU) {
    echo "La palabra contiene todas las vocales";
} else {
    echo "No contiene todas las vocales";
}

?>